require 'socket'
Dir.glob('lib/**/*.rb').each do |file|
  require file
end

module WebServer
  class Server
    attr_accessor :httpd, :mime, :logger

    DEFAULT_PORT = 2468
    CONFIG_FOLDER = "config/"
    CONFIG_FILE = "httpd.conf"
    MIME_FILE = "mime.types"

    LOGGER_FILE = "log.txt"
    LOGGER_FOLDER = "./"

    def initialize(options={})
      # Set up WebServer's configuration files and logger here
      # Do any preparation necessary to allow threading multiple requests
      
      # Create and open our file
      # HTTP
      http_opt = File.join(CONFIG_FOLDER, CONFIG_FILE)
      httpd_file = File.new(http_opt, 'rb')
      @httpd = HttpdConf.new(httpd_file)

      #MIME
      mime_opt = File.join(CONFIG_FOLDER, MIME_FILE)
      mime_file = File.new(mime_opt, 'rb')
      @mime = MimeTypes.new(mime_file)
    
      #LOGGER
      logger_opt = File.join(LOGGER_FOLDER, LOGGER_FILE)
      @logger = Logger.new(logger_opt, options)
    end

    def start
      # Begin your 'infinite' loop, reading from the TCPServer, and
      # processing the requests as connections are made
      
      puts "Starting web_server.rb: PORT: #{@httpd.port}"
      server = TCPServer.new('localhost', @httpd.port)  # Socket to listen on port 2000
      loop do                       # Servers run forever

          Thread.start(server.accept) do |socket|
            new_worker = Worker.new(socket, self)
          end

      end
         
    end 
  end
end

WebServer::Server.new.start