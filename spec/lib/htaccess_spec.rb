require 'spec_helper'


describe WebServer::HtAccess do


  let(:htpasswd_file) do
    <<-FILE_CONTENT
      valid_name:valid_pwd
    FILE_CONTENT
  end

  let(:htaccess_file_name) {}
  let(:auth_name) {"This is the auth_name"}
  let(:htaccess_file) do
    <<-FILE_CONTENT
      AuthUserFile #{htaccess_file_name}
      AuthType Basic
      AuthName #{auth_name}
      Require valid-user
    FILE_CONTENT
  end

  #Two cases around require 'valid-user' which is any user in the file or a specific user 'jrob'

  let(:htaccess) { Webserver::HtAccess.new(htaccess_file) }


  describe '#auth_user_file' do
    it 'returns the AuthUserFile string' do
      expect(htaccess.auth_user_file).to eq htaccess_file_name
    end
  end

  describe '#auth_type' do
    it 'returns "Basic"' do
      expect(htaccess.auth_type).to eq auth_name
    end
  end

  describe '#require_users' do
    it 'returns the Require (users who should be checked) string' do
      expect(htaccess.require_users).to eq 'valid-user'
    end
  end

  describe '#authorized?' do
    context 'when credentials are invalid' do
      it 'returns false'
    end
    it 'returns true for a valid credential'

    context 'when credentials are valid' do
      it 'returns true'

    end
  end


end