# The Request class encapsulates the parsing of an HTTP Request
require 'uri'
require 'socket'

module WebServer
  class Request
    attr_accessor :http_method, :uri, :version, :headers, :body, :params, :parsed_request

    # Request creation receives a reference to the socket over which
    # the client has connected
    def initialize(socket)
      @parsed_request = false
      @entire_message = ""

      @header_string = ""
      @body_string = ""
      @headers = {}
      @body = ""
      @params = {}

      @HTTP_ACTIONS = ["GET", "HEAD", "POST", "PUT"]
      @TOTAL_URI

      # Perform any setup, then parse the request
      @stored_socket = socket
      parse
    end

    def parsed_request?
      return @parsed_request
    end

    def auth_present?
      if (ENV["AUTHORIZATION"] != nil)
        return true
      end
      return false
    end

    def modified_present?
      if (ENV["IF_MODIFIED_SINCE"] != nil)
        return true
      end
      return false
    end

    # Parse the request from the socket - Note that this method takes no
    # parameters
    def parse
      @entire_message = @stored_socket.recv(1024)

      split_message = @entire_message.split(/^\s$/)
      #Process header
      if (!split_message.empty?)
        @parsed_request = true
        @header_string = split_message[0].strip
        @header_string.each_line do |head_line|
          parse_header(head_line)
        end

        #If we have a Body then process body
        if (split_message.length > 1)
          @body_string = split_message[1].strip
          @body_string.each_line do |body_line|
            parse_body(body_line)
          end
        end
      end

    end

    def parse_header(header_line)
      split_line = header_line.split(' ')
      if (@HTTP_ACTIONS.include? split_line[0])
        #HTTP action
        @http_method = split_line[0]
        @version = split_line[2]
        @TOTAL_URI = URI(split_line[1])
        @uri = @TOTAL_URI.path
        if (!@TOTAL_URI.query.nil?)
          parse_params(@TOTAL_URI.query)
        end

      # Ensure header line isn't an invalid request method
      elsif (!header_line.include? "HTTP/")
        matched = /(.*):\s(.*)/.match(header_line).captures
        @headers[matched[0].upcase.tr('-', '_')] = matched[1]
        ENV[matched[0].upcase.tr('-', '_')] = matched[1]
      end
    end

    def parse_body(body_line)
      if (@http_method == "POST")
        parse_params(body_line)
      end
        @body << body_line
    end

    def parse_params(line)
      #Split each params by '&' if one is present, otherwise no params
      #We have a & so split the line based on it
      split_params = line.split("&")

      split_params.each do |param|
        add_param(param)
      end


    end

    def add_param(line)

      if (line.include?('='))
        split_line = line.split('=')
        @params[split_line[0]] = split_line[1]
      end

    end

    # @return [line]
    def trim_quotes(line)
      line.tr(" '\"", '')
    end

    def trim_colon(line)
      line.tr(':', '')
    end
  end
end
