require_relative 'response/base'
require_relative 'response/bad_request'
require_relative 'response/forbidden'
require_relative 'response/not_found'
require_relative 'response/not_modified'
require_relative 'response/ok'
require_relative 'response/server_error'
require_relative 'response/successfully_created'
require_relative 'response/unauthorized'

module WebServer
  module Response
    DEFAULT_HTTP_VERSION = 'HTTP/1.1'

    RESPONSE_CODES = {
        200 => 'OK',
        201 => 'Successfully Created',
        304 => 'Not Modified',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not Found',
        500 => 'Internal Server Error'
    }

    @HTTP_ACTIONS = ["GET", "HEAD", "POST", "PUT"]

    def self.default_headers
      {
          'Date' => Time.now.strftime('%a, %e %b %Y %H:%M:%S %Z'),
          'Server' => 'John Roberts CSC 667'
      }
    end

    module Factory
      def self.create(resource)
        # Decide if resource is found/not found, auth/noauth, etc and make relevant Response

        @read_msg = ""
        # Get resources action
        if (resource.request.http_method.eql? "GET")
          self.process_get(resource)
        elsif (resource.request.http_method.eql? "POST")
          self.process_post(resource)
        elsif (resource.request.http_method.eql? "PUT")
          self.process_put(resource)
        elsif (resource.request.http_method.eql? "HEAD")
          self.process_head(resource)
        else
          return BadRequest.new(resource)
        end
      end

      def self.error(resource, error_object)
        WebServer::Response::ServerError.new(resource, exception: error_object)
      end

      def self.execute_script(resource, env_var)
        output = ''
        IO.popen([env_var, resource.get_resolved_path]) { |f| f.each { |line| output << line } }
        return output
      end

      def self.read_in_file(resolved_path)
        #Read in file, return handle
        found_resource = File.new(resolved_path, "rb")
        read_contents = ""
        found_resource.each_line do |line|
          read_contents << line
        end
        return read_contents
      end


      # Logical sequencing for GET, HEAD, POST, & PUT methods #


      def self.process_head(resource)
        if (resource.protected?)
          if (resource.request.auth_present?)

            # Ensure credentials match
            auth = ENV["AUTHORIZATION"]
            if (resource.authorized?(auth))
              @resolved_path = resource.get_resolved_path.clone
              if (File.exists?(@resolved_path))

                # Execute script and pipe output to response
                if (resource.script_aliased?)
                  @read_msg = self.execute_script(resource, resource.request.params)
                  return CgiOk.new(resource, @read_msg)

                  # If requested, determine if file has been modified
                elsif (resource.request.modified_present?)
                  file_date = File.mtime(@resolved_path)
                  header_date = DateTime.parse(ENV["IF_MODIFIED_SINCE"]).to_time
                  if (header_date > file_date)
                    return NotModified.new(resource)
                  end
                end
                return Ok.new(resource)
              else
                return NotFound.new(resource)
              end
            else
              return Forbidden.new(resource)
            end
          else
            return Unauthorized.new(resource)
          end
        else
          # File is unprotected, just determine it exists
          @resolved_path = resource.get_resolved_path.clone
          if (File.exists?(@resolved_path))
            if (resource.script_aliased?)

              #Execute script and pipe output to response
              @read_msg = self.execute_script(resource, resource.request.params)
              return CgiOk.new(resource, @read_msg)

              # If requested, determine if file has been modified
            elsif (resource.request.modified_present?)
              file_date = File.mtime(@resolved_path)
              header_date = DateTime.parse(ENV["IF_MODIFIED_SINCE"]).to_time
              if (header_date > file_date)
                return NotModified.new(resource)
              end
            end
            return Ok.new(resource)
          else
            return NotFound.new(resource)
          end
        end
      end


      def self.process_put(resource)
        if (resource.protected?)
          if (resource.request.auth_present?)

            # Ensure credentials match
            auth = ENV["AUTHORIZATION"]
            if (resource.authorized?(auth))
              @resolved_path = resource.get_resolved_path.clone

              # Create file with contents provided
              if (File.exists?(@resolved_path))
                File.delete(@resolved_path)
              end
              File.open(@resolved_path, 'w') { |f| f.write(resource.request.body) }
              return SuccessfullyCreated.new(resource, @contents)
            else
              return Forbidden.new(resource)
            end
          else
            return Unauthorized.new(resource)
          end
        else
          # Create unprotected file with contents provided
          @resolved_path = resource.get_resolved_path.clone
          if (File.exists?(@resolved_path))
            File.delete(@resolved_path)
          end
          File.open(@resolved_path, 'w') { |f| f.write(resource.request.body) }
          return SuccessfullyCreated.new(resource)
        end
      end


      def self.process_get(resource)
        if (resource.protected?)
          @read_msg = ""
          if (resource.request.auth_present?)

            # Ensure credentials match
            auth = ENV["AUTHORIZATION"]
            if (resource.authorized?(auth))
              @resolved_path = resource.get_resolved_path
              if (File.exists?(@resolved_path))
                if (resource.script_aliased?)

                  # Execute script and pipe output to response
                  @read_msg = self.execute_script(resource, resource.request.params)
                  return CgiOk.new(resource, @read_msg)

                  # If requested, determine if file has been modified
                elsif (resource.request.modified_present?)
                  file_date = File.mtime(@resolved_path)
                  header_date = DateTime.parse(ENV["IF_MODIFIED_SINCE"]).to_time
                  if (header_date > file_date)
                    return NotModified.new(resource)
                  end
                end
                @read_msg = self.read_in_file(@resolved_path)
                return Ok.new(resource, @read_msg)
              else
                return NotFound.new(resource)
              end
            else
              return Forbidden.new(resource)
            end
          else
            return Unauthorized.new(resource)
          end
        else

          # Create unprotected file with contents provided
          @resolved_path = resource.get_resolved_path.clone
          if (File.exists?(@resolved_path))
            if (resource.script_aliased?)

              # Execute script and pipe output to response
              @read_msg = self.execute_script(resource, resource.request.params)
              return CgiOk.new(resource, @read_msg)

              # If requested, determine if file has been modified
            elsif (resource.request.modified_present?)
              file_date = File.mtime(@resolved_path)
              header_date = DateTime.parse(ENV["IF_MODIFIED_SINCE"]).to_time
              if (header_date > file_date)
                return NotModified.new(resource)
              end
            end
            @read_msg = self.read_in_file(@resolved_path)
            return Ok.new(resource, @read_msg)
          else
            return NotFound.new(resource)
          end
        end
      end


      def self.process_post(resource)
        @read_msg = ""
        if (resource.protected?)
          if (resource.request.auth_present?)

            # Ensure credentials match
            auth = ENV["AUTHORIZATION"]
            if (resource.authorized?(auth))
              @resolved_path = resource.get_resolved_path
              if (File.exists?(@resolved_path))
                if (resource.script_aliased?)

                  # Execute script and pipe output to response
                  @read_msg = self.execute_script(resource, resource.request.params)
                  return CgiOk.new(resource, @read_msg)
                else
                  # Don't open file, just return OK
                  return Ok.new(resource, @read_msg)
                end
              else
                return NotFound.new(resource)
              end
            else
              return Forbidden.new(resource)
            end
          else
            return Unauthorized.new(resource)
          end
        else

          # Get unprotected file with contents provided
          @resolved_path = resource.get_resolved_path
          if (File.exists?(@resolved_path))
            if (resource.script_aliased?)

              # Execute script and pipe output to response
              @read_msg = self.execute_script(resource, resource.request.params)
              return CgiOk.new(resource, @read_msg)
            else
              @read_msg = self.read_in_file(@resolved_path)
              return Ok.new(resource, @read_msg)
            end
          else
            return NotFound.new(resource)
          end
        end
      end
    end
  end
end
