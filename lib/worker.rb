require_relative 'request'
require_relative 'response'


# This class will be executed in the context of a thread, and
# should instantiate a Request from the client (socket), perform
# any logging, and issue the Response to the client.
module WebServer
  class Worker
    attr_accessor :socket, :web_server, :res, :req

    # Takes a reference to the client socket and the logger object
    def initialize(client_socket, server)
      #begin
        @socket = client_socket
        @web_server = server
        #@request = WebServer::Request.new(@socket)
        if (request.parsed_request?)
          @resource = Resource.new(request, @web_server.httpd, @web_server.mime)

          #@response = Response::Factory.create(@resource)

          #Log the response
          @web_server.logger.log(request, response)

          #Send back the response
          @socket.puts response
          @socket.flush
          puts response
        #rescue Exception => e
          #Generate 500 Response
         # @res = WebServer::ServerError.new(@resource, {})
          #@socket.puts @res.message
        #ensure
          @socket.close()
        #end
        end

    end

    def response
      @res ||= WebServer::Response::Factory.create(@resource).message
    end

    def request
      @req ||= WebServer::Request.new(@socket)
    end
  end
end
