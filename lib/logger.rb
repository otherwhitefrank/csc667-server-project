module WebServer
  class Logger

    # Takes the absolute path to the log file, and any options
    # you may want to specify.  I include the option :echo to 
    # allow me to decide if I want my server to print out log
    # messages as they get generated
    def initialize(log_file_path, options={})
        @logger_file = File.new(log_file_path, 'w')
    end

    # Log a message using the information from Request and 
    # Response objects
    def log(request, response)
        if (!@logger_file.nil?)
            #do logging
                
            #Request
            if (!request.nil? and !response.nil?)
              #:http_method, :uri, :version, :headers, :body, :params
              @logger_file.write("Request: " + request.http_method.to_s)

              #Response
              #:version, :code, :body
              @logger_file.write("Response: " + response)
            end

        else
            puts "Log file not open!"
        end
    end

    # Allow the consumer of this class to flush and close the 
    # log file
    def close
        @logger_file.close
    end
  end
end
