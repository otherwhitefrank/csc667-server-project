require_relative 'configuration'

# Parses, stores and exposes the values from the mime.types file
module WebServer
  class MimeTypes < Configuration
    def initialize(options={})
    	@mimes = {}

        super(options)
    end

    def parse_line(line)
        #A function that checks a single line against various regexp's. If it find a match it sets the relevant instance variable
        #otherwise the line is ignored
        
    	arr_tokens = line.split()
    	
     	#MimeType is the first entry of our array
     	mime_type = arr_tokens[0]

    	arr_tokens.each do |pattern|
    		if (pattern != arr_tokens[0])
    			mime_ext = pattern
    			@mimes[mime_ext] = mime_type
    		end
    	end        
    end
    
    # Returns the mime type for the specified extension
    def for_extension(extension)
        if (!@mimes[extension].nil?)
    	   @mimes[extension]
        else
            return "text/plain"
        end
    end
  end
end
