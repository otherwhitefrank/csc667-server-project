require_relative 'configuration'

# Parses, stores, and exposes the values from the httpd.conf file
module WebServer
  class HttpdConf < Configuration
    def initialize(options={})
        #HttpdConf is passed in a File of the opened httpd.conf file. It then parses each line and updates the relevant 
        #local variables that can later be queried by a getter function.

        #define object variables for regex matchers
        @configs = {}

        #Maybe change to hashmap?
        @aliases = {}

        @script_aliases = {}
       
        super(options)
    end

    def parse_line(line)
        #A function that checks a single line against various regexp's. If it find a match it sets the relevant instance variable
        #otherwise the line is ignored
        
        split_line = line.split()

        first_token = split_line[0]

        if (first_token == "Alias")
            found_alias = split_line[1]
            found_dest = split_line[2]
            @aliases[found_alias] = trim_quotes(found_dest)
        elsif (first_token == "ScriptAlias")
            found_alias = split_line[1]
            found_dest = split_line[2]
            @script_aliases[found_alias] = trim_quotes(found_dest)
        elsif (first_token == "Listen")
            #Handle Listen: <Port>
            @configs[first_token] = split_line[1].to_i
        else
            #Handle all other tokens
            @configs[first_token] = trim_quotes(split_line[1].to_s)
        end
        
    end
    

    # Returns the value of the ServerRoot
    def server_root 
        @configs["ServerRoot"]
    end

    # Returns the value of the DocumentRoot
    def document_root 
        @configs["DocumentRoot"]
    end

    # Returns the directory index file
    def directory_index
      if (@configs["DirectoryIndex"])
        return @configs["DirectoryIndex"]
      else
        return "index.html"
      end

    end

    # Returns the *integer* value of Listen
    def port
        if (@configs["Listen"])
          return @configs["Listen"]
        else
          return 8096
        end
    end

    # Returns the value of LogFile
    def log_file
        @configs["LogFile"]
    end

    # Returns the name of the AccessFile 
    def access_file_name
        if (@configs["AccessFileName"])
          @configs["AccessFileName"]
        else
          return ".htaccess"
        end
    end

    # Returns an array of ScriptAlias directories
    def script_aliases
        @script_aliases.keys
    end

    # Returns the aliased path for a given ScriptAlias directory
    def script_alias_path(path)
        if (@script_aliases.keys.include? path)
            @script_aliases[path].clone
        end
    end

    # Returns an array of Alias directories
    def aliases
        @aliases.keys
    end

    # Returns the aliased path for a given Alias directory
    def alias_path(path)
        if (@aliases.keys.include? path)
            @aliases[path].clone
        end
    end
  end
end
