# This class should be used to encapuslate the functionality 
# necessary to open and parse configuration files. See
# HttpdConf and MimeTypes, both derived from this parent class.
module WebServer
  class Configuration
    def initialize(options={})
        if (!options.nil?)   
            options.each_line do |line|
                if (!comment_or_empty?(line))
                    parse_line(line)
                end
            end
        end
    end

    def parse_line(line)
    	#This is inherited by children classes
    end

    def comment_or_empty?(line)
        return ((line.include? "#") || (line.empty?))
    end

    def trim_quotes(line)
        return line.tr(" '\"", "")
    end
  end
end
