#Parses a .htaccess file and then parses associated .htpasswd file, responds to queries to check credentials
require_relative('config/configuration')

module WebServer
  class HtAccess < Configuration
    attr_accessor :file, :htaccess_headers, :htpasswd_headers

    def initialize()
      @auth_user_file = ""
      @auth_type = ""
      @auth_name = ""
      @authorizations = {}
    end

    def authorized?(header)

    end

    def parse_htaccess

    end

    def parse_htpasswd

    end

  end
end