require 'find'
require 'pathname'
require 'base64'

module WebServer
  class Resource
    attr_reader :request, :conf, :mimes, :htpasswd, :htaccess

    # @param [Object] mimes
    def initialize(request, httpd_conf, mimes)
      @request = request
      @conf = httpd_conf
      @mimes = mimes
      @htaccess = {}
      @htpasswd = {}

      @script_alias_found = FALSE
      @alias_found = FALSE

    end

    def resolve
      directories = @request.uri.split("/")
      solo_dir = ''
      curr_dir = ''
      full_path = @conf.document_root.clone

      directories.each do |partial_path|


        if (!partial_path.strip.empty?)  #Ignore whitespace
          solo_dir = "/" << partial_path.strip.to_s << "/"
          curr_dir << "/" << partial_path.strip.to_s

          if (defined? @conf.aliases)
            if (@conf.aliases.include? solo_dir)
              # Use alias path
              curr_dir = @conf.alias_path(solo_dir).clone
              @alias_found = TRUE
            end
          end

          if (defined? @conf.script_aliases)
            if (@conf.script_aliases.include? solo_dir)
            # Use script aliased path
            curr_dir = @conf.script_alias_path(solo_dir)
            @script_alias_found = TRUE
            end
          end

        end
      end
      if(!full_path.include? ".")
        curr_dir.slice!(0)
        full_path << curr_dir
      end

      ## Determine whether or not to use index.html
      if (!full_path.include? ".")
        if(!@script_alias_found)
        full_path << "/" + @conf.directory_index.to_s
        end
        end
      return full_path
    end

    def aliased?
      self.resolve
      return @alias_found
    end

    def script_aliased?
      self.resolve
      return @script_alias_found
    end

    def parse_access_file(access_file_path)
      #Read in the access file and
      if (File.exists?(access_file_path))
        access_file = File.open(access_file_path, 'rb')
        access_file.each_line do |line|
          tokens = line.split()
          @htaccess[tokens[0]] = tokens[1].tr('\"', '')
        end


        htpasswd_file = File.open(@htaccess['AuthUserFile'], "rb")

        htpasswd_file.each_line do |line|
          tokens = line.split(':')
          @htpasswd[tokens[0].strip] = tokens[1].strip
        end
      else
      end
    end


    def retrieve_access_file
      #Loop through each part of the URI and check for an .htaccess file
      @pn = Pathname.new(@request.uri)
      if(@pn.basename.to_s.include? ".")
        @split_path = @pn.dirname.to_s.split("/")
        @empty_path = @conf.document_root.to_s.clone
        @split_path.each do |directory|
          @empty_path << directory.to_s << '/'
          @check_file = @empty_path.dup
          @check_file << @conf.access_file_name.to_s
          if (File.exists?(@check_file))
            return @check_file
          end
        end
      else
        @split_path = @pn.basename.to_s
        @empty_path = @conf.document_root.to_s.clone
        @empty_path << @split_path.to_s << '/'
        @check_file = @empty_path.dup
        @check_file << @conf.access_file_name.to_s
        if (File.exists?(@check_file))
          return @check_file
        end
      end

      return nil
    end


      def protected?
      #Loop through each piece of the @request.uri and check for AccessFileName's presence
      #If found return true and possibly store the highest level wanted credentials, if not found return false

      access_file_path = retrieve_access_file
      if (access_file_path == nil)
        return false
      else
        return true
      end
    end

    def valid_credentials(credentials)
      #check credentials, hash has form
      #{username: 'valid_name', password: 'valid_pwd'}
      split_cred = credentials.split
      to_decode = split_cred[1]
      userName, password = Base64.decode64(to_decode).split(':')
      if (@htpasswd.has_key? userName)
        if (@htpasswd[userName] === password)
          return true
        else
          return false
        end
      else
        return false
      end
    end



    def get_resolved_path
      self.resolve
    end

    def authorized?(credentials)
      if self.protected?
        #URI is protected so check credentials
        access_file_path = retrieve_access_file
        self.parse_access_file(access_file_path)

        if (valid_credentials(credentials))
          return true
        else
          return false
        end
      else
        #No access file so return true
        return true
      end
    end
  end
end