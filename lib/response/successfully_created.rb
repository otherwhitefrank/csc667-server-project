module WebServer
  module Response
    # Class to handle 201 responses
    class SuccessfullyCreated < Base
      def initialize(resource, options={})
        super(resource, options)
        @code = resource.request.http_method
        @body = "<html><head><title>Successfully Created</title></head><body>The resource was created successfully.</body><html>"
      end

      def message
        response_string = ""
        response_string << @version
        response_string << " 201 "
        response_string << "#{RESPONSE_CODES[201]}\n"
        response_string << "#{WebServer::Response::default_headers['Date']}\n"
        response_string << "#{WebServer::Response::default_headers['Server']}\n"
        response_string << "Content-Type: text/html\n"
        response_string << "Content-Length: #{@body.length}\n\n"
        response_string << "#{@body}"

        return response_string
      end
    end
  end
end
