module WebServer
  module Response
    # Class to handle 404 errors
    class NotFound < Base
      def initialize(resource, options={})
        super(resource, options)
        @code = 404
        @body = "<html><head><title>Not Found</title></head><body>Sorry, the object you requested was not found.</body><html>"
      end

      def message
        #HTTP/1.1 404 Not Found
        #Content-Length: 1635
        #Content-Type: text/html
        #Server: Microsoft-IIS/6.0
        #X-Powered-By: ASP.NET
        #Date: Tue, 04 May 2010 22:30:36 GMT
        #Connection: close

        #def self.default_headers
        #  {
        #      'Date' => Time.now.strftime('%a, %e %b %Y %H:%M:%S %Z'),
        #      'Server' => 'John Roberts CSC 667'
        #  }
        #end

        response_string = ""
        response_string << "#{WebServer::Response::DEFAULT_HTTP_VERSION} #{@code} #{WebServer::Response::RESPONSE_CODES[404]}\n"
        response_string << "Content-type: text/html\n"
        response_string << "Content-length: #{@body.length}\n"
        response_string << "#{WebServer::Response::default_headers['Date']}\n"
        response_string << "#{WebServer::Response::default_headers['Server']}\n"
        response_string << "\n"
        response_string << "#{@body}"


        return response_string
      end
    end
  end
end
