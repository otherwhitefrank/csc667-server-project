module WebServer
  module Response
    # Provides the base functionality for all HTTP Responses 
    # (This allows us to inherit basic functionality in derived responses
    # to handle response code specific behavior)
    class Base
      attr_reader :version, :code, :body

      def initialize(resource, options={})
        @executing_script = false
        @resource = resource
        @version = resource.request.version
      end

      def message
      end

      def content_length
        @body.length
      end

      def content_type
        ext = File.extname(@resource.request.uri).tr('.', '')
        if (ext.empty?)
          return "text/html\n"
        else
          @resource.mimes.for_extension(ext)
        end
      end

    end
  end
end
