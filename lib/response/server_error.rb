module WebServer
  module Response
    # Class to handle 500 errors
    class ServerError < Base
      def initialize(resource, options={})
        super(resource, options)
        @code = resource.request.http_method
        @body = "<html><head><title>Server Error</title></head><body>There is a problem with the resource you are looking for, and it cannot be displayed.</body><html>"
      end

      def message
        response_string = ""
        response_string << @version
        response_string << " 500 "
        response_string << "#{RESPONSE_CODES[500]}\n"
        response_string << "#{WebServer::Response::default_headers['Date']}\n"
        response_string << "#{WebServer::Response::default_headers['Server']}\n"
        response_string << "Content-Type: text/html\n"
        response_string << "Content-Length: #{@body.length}\n\n"
        response_string << "#{@body}"

        return response_string
      end

    end
  end
end
