module WebServer
  module Response
    # Class to handle 200 OK responses
    class Ok < Base
      def initialize(resource, options={})
        @version = resource.request.version
        @code = resource.request.http_method
        @body = options
        super(resource, options)
      end

      def message
        response_string = ""
        response_string << @version
        response_string << " 200 "
        response_string << "#{RESPONSE_CODES[200]}\n"
        response_string << "#{WebServer::Response::default_headers['Date']}\n"
        response_string << "#{WebServer::Response::default_headers['Server']}\n"
        if(@code == "GET")
          response_string << "Content-Length: "
          response_string << "#{self.content_length}\n"
          response_string << "Content-Type: "
          response_string << "#{self.content_type}\n"
          response_string << "\n"
          response_string << @body
        elsif(@code == "HEAD")
          response_string << "Content-Length: "
          response_string << "#{self.content_length}\n"
          response_string << "Content-Type: "
          response_string << "#{self.content_type}\n\n"
        elsif(@code == "POST")
          response_string << "Content-Length: "
          response_string << "#{self.content_length}\n"
          response_string << "Content-Type: "
          response_string << "#{self.content_type}\n"
          response_string << "\n"
          response_string << @body
        end

        return response_string
      end

    end
  end
end
