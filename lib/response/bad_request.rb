module WebServer
  module Response
    # Class to handle 400 responses
    class BadRequest < Base
      def initialize(resource, options={})
        super(resource, options)
        @code = resource.request.http_method
        @body = "<html><head><title>Bad Request</title></head><body>Oops!  Looks like an invalid request method was provided.</body><html>"
      end

      def message
        response_string = ""
        response_string << "400 "
        response_string << "#{RESPONSE_CODES[400]}\n"
        response_string << "#{WebServer::Response::default_headers['Date']}\n"
        response_string << "#{WebServer::Response::default_headers['Server']}\n"
        response_string << "Content-Type: text/html\n"
        response_string << "Content-Length: #{@body.length}\n\n"
        response_string << "#{@body}\n\n"

        return response_string
      end
    end
  end
end
