require 'date'

module WebServer
  module Response
    # Class to handle 304 responses
    class NotModified < Base
        def initialize(resource, options={})
          super(resource, options)
          @code = resource.request.http_method
          @body = "<html><head><title>Not Modified</title></head><body>The resource has not been modified.</body><html>"
        end

        def message
          response_string = ""
          response_string << @version
          response_string << " 304 "
          response_string << "#{RESPONSE_CODES[304]}\n"
          response_string << "#{WebServer::Response::default_headers['Date']}\n"
          response_string << "#{WebServer::Response::default_headers['Server']}\n"
          response_string << "Content-Type: text/html\n"
          response_string << "Content-Length: #{@body.length}\n"
          response_string << "Expires: #{(DateTime.now + 1).httpdate}\n\n"

          return response_string
        end
    end
  end
end
