module WebServer
  module Response
    # Class to handle 401 responses
    class Unauthorized < Base
      def initialize(resource, options={})
        super(resource, options)
        @code = resource.request.http_method
      end

      def message
        response_string = ""
        response_string << @version
        response_string << " 401 "
        response_string << "#{RESPONSE_CODES[401]}\n"
        response_string << "#{WebServer::Response::default_headers['Date']}\n"
        response_string << "#{WebServer::Response::default_headers['Server']}\n"
        response_string << "Content-Type: text/html\n"
        response_string << "Content-Length: 0\n"
        response_string << "WWW-Authenticate: Basic\n\n"

        return response_string
      end

    end
  end
end
